#require 'byebug'
#debugger
class HumanPlayer
  attr_accessor :player, :board
  def initialize(player)
    @player = player
  end

  def display(board)
    @board = board
  end

  def setup
  end

  def get_play
    puts "Enter your move (ex: 0, 1)"
    move = gets.chomp
    move = move.split(", ").map(&:to_i)
  end
end
