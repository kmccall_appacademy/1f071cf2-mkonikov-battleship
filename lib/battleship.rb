require_relative 'board'
require_relative 'player'
require_relative 'computer'
require_relative 'ship'


class BattleshipGame
  attr_reader :player_one, :board, :player_one_board, :player_two, :player_two_board

  def initialize(player_one, player_one_board, two_player = false)
    @player_one = player_one
    @player_one_board = player_one_board
    @board = player_one_board
    @player_two = ComputerPlayer.new("Computer")
    @player_two_board = Board.new
    @two_player = two_player
    @current_player = player_one
    @current_board = @player_two_board
  end

  def switch_players!
    self.current_player = @current_player == @player_one ? @player_two : @player_one
    self.current_board = @current_board == @player_one_board ? @player_two_board : @player_one_board
  end

  def setup
    player_one.setup
    player_two.setup
  end

  def attack(pos)
    @two_player ? @current_board[pos] = :x : @player_one_board[pos] = :x
  end

  def play
    setup if @two_player
    play_turn until won?
    puts "Yay! You won!"
  end

  def play_turn
    player = @two_player ? @current_player : @player_one
    board = @two_player ? @current_board : @player_one_board
    player.display(@current_board) if @two_player
    move = player.get_play
    attack(move)
    display_status
    switch_players! if @two_player
  end

  def count
    board = @two_player ? @current_board : @player_one_board
    board.count
  end

  def won?
    @player_one_board.won? || @player_two_board.won?
  end

  def game_over?
    @player_one_board.won? || @player_two_board.won?
  end

  def display_status
    puts "There are #{@player_one_board.count} ships remaining on player one's board."
    puts "There are #{@player_two_board.count} ships remaining on player two's board." if @two_player
  end

end

#mendel = HumanPlayer.new("Mendel")
#board = Board.new(Array.new(3) { Array.new(3) })
#hasboro = BattleshipGame.new(mendel, board)

#hasboro.play
