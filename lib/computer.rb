class ComputerPlayer
  attr_reader :player, :board

  BOATS = { carrier: 5, battleship: 4, submarine: 3, destroyer: 3, patrol: 2 }

  def initialize(player)
    @player = player
    @past_moves = []
  end

  def display(board)
    @board = board
  end

  def find_pos(length)


  def setup
    #INCOMPLETE
  end

  def random_move
    position = [rand(board.grid.length), rand(board.grid[0].length)]
    random_move if past_moves.any? { |el| el == position }
    position
  end

  def get_play
    position
  end
end
