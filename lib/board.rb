#require 'byebug'
class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def in_range?(pos)
    pos[0] <= @grid.length && pos[1] <= @grid.length
  end

  def populate_grid
    5.times { place_random_ship }
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    return true if count == 0 && pos == nil
    return false if pos == nil
    self[pos].nil?
  end

  def place_random_ship
    raise "Error" if full?
    position = [rand(@grid.length), rand(@grid.length)]
    #if empty?(position)
    if empty?(position)
      self[position] = :s
    else
     puts "Not empty"
    end
  end

  def full?
    @grid.flatten.none? { |cell| cell.nil? }
  end

  def won?
    count == 0
  end

end
